#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'FLoC PC Workshop Committee'
SITENAME = 'FLoC Proof Complexity Workshop'
SITEURL = ''

TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

PLUGIN_PATHS = ['plugins']
PLUGINS = [] #['pelican-page-hierarchy']
TYPOGRIFY = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'themes/simple-bootstrap'

PAGE_PATHS = ['']
PAGE_EXCLUDES = ['extra']

READERS = {'html': None}

ARTICLE_PATHS = []

STATIC_PATHS = ['images', 'extra']

EXTRA_PATH_METADATA = {}

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
PAGE_ORDER_BY = 'menu_order'
